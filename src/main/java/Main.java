import model.Person;
import model.PhoneBook;
import model.PhoneBookService;
import model.contact.Contact;
import model.contact.ContactType;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;


public class Main {

    private static final Scanner scanner = new Scanner(System.in);

    private static final PhoneBookService phoneBook = new PhoneBook();
    private static final Display display = new Display();

    public static void main(String[] args) {

        int option;

        while (true) {

            display.displayMenu();

            System.out.print("Введите номер опции: ");

            if (scanner.hasNextInt()) {

                option = scanner.nextInt();

                switch (option) {

                    case 0:

                        System.exit(0);

                    case 1:

                        System.out.print("Введите имя: ");
                        String firstName = scanner.next();

                        System.out.print("Введите фамилию: ");
                        String lastName = scanner.next();

                        Person person = new Person(firstName, lastName);

                        List<Contact> contacts = new ArrayList<>();

                        while (true) {

                            try {

                                display.displayContactTypeChoice();

                                System.out.print("Тип контакта: ");

                                int typeIndex = scanner.nextInt() - 1;

                                if (typeIndex == -1) break;

                                try {

                                    System.out.print("\nЗначение контакта: ");

                                    Contact contact = new Contact(ContactType.values()[typeIndex], scanner.next());

                                    if (!(phoneBook.exists(contact) || contacts.contains(contact))) contacts.add(contact);
                                    else System.out.println("\nКонтакт уже существует\n");

                                } catch (IndexOutOfBoundsException exception) {
                                    System.out.println("\nНеправильный тип контакта\n");
                                }

                            } catch (InputMismatchException exception) {

                                scanner.nextLine();

                                System.out.println("\nНеправильный тип контакта\n");

                            }

                        }

                        person.setContacts(contacts);

                        phoneBook.add(person);

                        break;

                    case 2:

                        display.displayPersons(phoneBook.sort());

                        break;

                    case 3:

                        display.displayPersons(phoneBook.filter(ContactType.PHONE));

                        break;

                    case 4:

                        display.displayPersons(phoneBook.filter(ContactType.EMAIL));

                        break;

                    case 5:

                        System.out.print("Введите имя (часть имени): ");

                        display.displayPersons(phoneBook.searchByName(scanner.next()));

                        break;

                    case 6:

                        System.out.print("Введите начало контакта: ");

                        display.displayPersons(phoneBook.searchByContact(scanner.next()));

                        break;

                    case 7:

                        System.out.print("Введите значение контакта: ");

                        if (phoneBook.delete(scanner.next()))
                            System.out.println("Контакт успешно удален\n");
                        else System.out.println("Нет контакта с указанным значением\n");

                        break;

                    default:

                        System.out.println("\nНеправильно введена опция\n");

                        break;

                }

            } else {

                scanner.nextLine();

                System.out.println("\nНеправильно введена опция\n");

            }

        }

    }

}
