package model.contact;

import lombok.Data;
import lombok.RequiredArgsConstructor;


@Data
@RequiredArgsConstructor
public class Contact {

    private final ContactType type;
    private final String value;

    @Override
    public String toString() {
        return String.format("%s: %s", this.type, this.value);
    }

}
